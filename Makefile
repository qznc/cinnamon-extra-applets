APPLETURL=http://cinnamon-spices.linuxmint.com/uploads/applets/
APPLETS=GVOU-I2IS-DJV7 Y0AY-WGJM-4CQJ FTBL-QMQO-FG7Q 62G3-RSBX-58HS
NAME=cinnamon-extra-applets
VERSION=1
FULLNAME=${NAME}-${VERSION}

default: $(FULLNAME).tar.bz2

.zips_fetched: $(foreach x,$(APPLETS),zips/$x.zip) Makefile
	touch $@

zips/%.zip: Makefile
	mkdir -p zips
	cd zips && wget -c ${APPLETURL}$(shell basename $@)

.applets_extracted: .zips_fetched
	mkdir -p ${FULLNAME}
	cd ${FULLNAME} && $(foreach x,$(APPLETS),unzip ../zips/$x.zip;)
	touch $@

$(FULLNAME)/Makefile: inc/Makefile
	mkdir -p ${FULLNAME}
	ln $< $@

$(FULLNAME).tar.bz2: $(FULLNAME)/Makefile .applets_extracted
	tar cjf $@ ${FULLNAME}

.PHONY: clean

clean:
	rm -rf zips ${FULLNAME}
	rm -f .applets_extracted .zips_fetched

