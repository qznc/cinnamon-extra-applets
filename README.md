# Cinnamon Extra Applets

A collection of Cinnamon applets to add missing functionality. Note the
"missing". This collection does not intend to duplicate functionality.

## Installation

Wrong idea. This creates an installable package, but is not installed
itself. That said, build the package and install it.

## Build

A simple call to "make" should suffice.

Requires wget, unzip, tar.
